#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/uinput.h>

#define CLI_LENGTH_MAX 256
#define CLI_OPTION_MAX 10
#define WIN_W 1280
#define WIN_H 720

char command[CLI_LENGTH_MAX];
char *option[CLI_OPTION_MAX];
int opt_cnt;
int fd;

void emit(int fd, int type, int code, int val)
{
   struct input_event ie;

   ie.type = type;
   ie.code = code;
   ie.value = val;
   /* timestamp values below are ignored */
   ie.time.tv_sec = 0;
   ie.time.tv_usec = 0;

   write(fd, &ie, sizeof(ie));
}

void ui_set(int fd)
{
	struct uinput_user_dev uud;

	ioctl(fd, UI_SET_EVBIT, EV_KEY);
	ioctl(fd, UI_SET_KEYBIT, BTN_LEFT);

	ioctl(fd, UI_SET_EVBIT, EV_REL);
	ioctl(fd, UI_SET_RELBIT, REL_X);
	ioctl(fd, UI_SET_RELBIT, REL_Y);

	ioctl(fd, UI_SET_EVBIT, EV_ABS);
	ioctl(fd, UI_SET_ABSBIT, ABS_X);
	ioctl(fd, UI_SET_ABSBIT, ABS_Y);

	memset(&uud, 0, sizeof(uud));
	snprintf(uud.name, UINPUT_MAX_NAME_SIZE, "uinput");
	uud.absmax[ABS_X] = WIN_W;
	uud.absmin[ABS_X] = 0;
	uud.absfuzz[ABS_X] = 0;
	uud.absflat[ABS_X] = 0;
	uud.absmax[ABS_Y] = WIN_H;
	uud.absmin[ABS_Y] = 0;
	uud.absfuzz[ABS_Y] = 0;
	uud.absflat[ABS_Y] = 0;

	write(fd, &uud, sizeof(uud));
	ioctl(fd, UI_DEV_CREATE);
}

void cursor_move(int x, int y)
{
	emit(fd, EV_REL, REL_X, x);
	emit(fd, EV_REL, REL_Y, y);
	emit(fd, EV_SYN, SYN_REPORT, 0);
}

void cursor_set(int x, int y)
{
	emit(fd, EV_ABS, ABS_X, x);
	emit(fd, EV_ABS, ABS_Y, y);
	emit(fd, EV_SYN, SYN_REPORT, 0);
}

void parse_mouse_move(void)
{
	int x, y;

	if (opt_cnt != 4 || 'h' == option[2][0]) {
		help_mouse();
		return;
	}

	x = atoi(option[2]);
	y = atoi(option[3]);

	cursor_move(x, y);
}

void parse_mouse_set(void)
{
	int x, y;

	if (opt_cnt != 4 || 'h' == option[2][0]) {
		help_mouse();
		return;
	}

	x = atoi(option[2]);
	y = atoi(option[3]);

	cursor_set(x, y);
}

void parse_mouse_click(void)
{
	int x, y;

	if (opt_cnt == 4) {
		x = atoi(option[2]);
		y = atoi(option[3]);
	}

	switch (opt_cnt) {
		case 4:
			emit(fd, EV_ABS, ABS_X, x);
			emit(fd, EV_ABS, ABS_Y, y);
			emit(fd, EV_SYN, SYN_REPORT, 0);
		case 2:
			emit(fd, EV_KEY, BTN_LEFT, 1);
			emit(fd, EV_SYN, SYN_REPORT, 0);
			usleep(1000);
			emit(fd, EV_KEY, BTN_LEFT, 0);
			emit(fd, EV_SYN, SYN_REPORT, 0);
			break;
		default:
			option[2][0] = 'h';
			break;
	}

	if ('h' == option[2][0]) {
		help_mouse();
		return;
	}

	x = atoi(option[2]);
	y = atoi(option[3]);

	emit(fd, EV_REL, REL_X, 5);
	emit(fd, EV_REL, REL_Y, 5);
	emit(fd, EV_SYN, SYN_REPORT, 0);

}

void help(void)
{
	printf ("support commands:\nexit mouse\n");
}


void help_mouse(void)
{
	printf ("mouse click [x] [y]\n");
	printf ("mouse move x y\n");
	printf ("mouse set x y\n");
}

void parse_mouse(void)
{
	switch (option[1][0]) {
		case 'm':
			parse_mouse_move();
			break;
		case 's':
			parse_mouse_set();
			break;
		case 'c':
			parse_mouse_click();
			break;
		default:
			help_mouse();
			break;
	}
}

int parse_command()
{
	int i;

	#if 0
	for (i=0; i<opt_cnt; i++) {
		printf("[%d] %s\n", i, option[i]);
	}
	#endif

	switch (option[0][0]) {
		case 'e':	//exit
			return -1;
			break;
		case 'm':	//mouse
			parse_mouse();
			break;
		default:
			help();
			break;
	}
	return 0;
}

int main(void)
{
	int cnt;
	char ch;

	fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	if (fd < 0) {
		return fd;
	}
	ui_set(fd);

	while (1) {
		memset(command, 0, CLI_LENGTH_MAX);
		cnt = 0;
		opt_cnt = 0;
		option[opt_cnt++] = command;
		printf("Please enter your command:\n");
		while(read(STDIN_FILENO, &ch, 1) > 0) {
			if ('\n' == ch) {
				break;
			}
			if (' ' == ch) {
				option[opt_cnt++] = command + cnt + 1;
			} else {
				command[cnt] = ch;
			}
			if (CLI_LENGTH_MAX == ++cnt || opt_cnt == CLI_OPTION_MAX) {
				break;
			}
		}
		if (-1 == parse_command()) {
			break;
		}
	}

	ioctl(fd, UI_DEV_DESTROY);
	close(fd);

	printf("bye\n");
	return 0;
}
